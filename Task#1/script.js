// Start the fadeIn animation for element with class "fadeIn"
setInterval(function () {
    const fadeInElement = document.querySelector('.feature5 .fadeIn');
    fadeInElement.classList.remove('fadeIn');
    void fadeInElement.offsetWidth; // Trigger reflow
    fadeInElement.classList.add('fadeIn');
}, 2000); // Repeat every 2 seconds

// Start the slideDown animation for element with class "slideDown"
setInterval(function () {
    const slideDownElement = document.querySelector('.feature6 .slideDown');
    slideDownElement.classList.remove('slideDown');
    void slideDownElement.offsetWidth; // Trigger reflow
    slideDownElement.classList.add('slideDown');
}, 3000); // Repeat every 3 seconds